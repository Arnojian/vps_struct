
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

#include "server.h"

int openTcp(ServerInfo *pstServInfo)
{
	struct sockaddr_in servAddr ;
	struct sockaddr_in locAddr ;
	int fd = 0 ;
	int on = 1 ;
	int sendbufLen = 1024 ;
	int rcvbufLen = 1024 ;
	int flag = 0 ;
	
	memset(&locAddr,0x00,sizeof(locAddr)) ;

	locAddr.sin_family = AF_INET ;
	locAddr.sin_port = 0;
	locAddr.sin_addr.s_addr =  0;

	fd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	
	if(bind(fd,(struct sockaddr*)&locAddr,sizeof(locAddr))<0)
	{
		printf("%s:%d error bind open_test %s\n",__FILE__,__LINE__,strerror(errno));
		close(fd);
		return -1 ;
	}

	if(setsockopt(fd,SOL_SOCKET,SO_RCVBUF,(const char *)&rcvbufLen,sizeof(rcvbufLen))<0)
	{
		printf("%s:%d error setsockopt SO_RCVBUF %s\n",__FILE__,__LINE__,strerror(errno));
		close(fd);
		return -1 ;
	}

	if(setsockopt(fd,SOL_SOCKET,SO_SNDBUF,(const char *)&sendbufLen,sizeof(sendbufLen))<0)
	{
		printf("%s:%d error setsockopt SO_SNDBUF %s\n",__FILE__,__LINE__,strerror(errno));
		close(fd);
		return -1 ;
	}

	if(setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,(const char *)&on,sizeof(on))<0)
	{
		printf("%s:%d error setsockopt SO_REUSEADDR %s\n",strerror(errno)) ;
		close(fd);
		return -1 ;
	}
	
	flag = fcntl(fd,F_GETFL,0) ;
	if(flag < 0)
	{
		printf("%s:%d fcntl F_GETFL is err :%s\n",strerror(errno));
		close(fd);
		return -1 ;
	}
	flag |= O_NONBLOCK ;

	flag = fcntl(fd,F_SETFL,flag) ;
	if(flag <0)
	{
		printf("%s:%d fcntl F_SETFL is err:%s\n",strerror(errno));
		close(fd);
		return -1 ;
	}

	ServerParam *pstServParam = &pstServInfo->stParam ;
	servAddr.sin_family = AF_INET ;
	servAddr.sin_port = htons(pstServParam->iPort);
	servAddr.sin_addr.s_addr = inet_addr(pstServParam->acIP);

	if(connect(fd,(struct sockaddr*)&servAddr,sizeof(servAddr))<0)
	{
		if(errno != EINPROGRESS)
		{
			printf("%s:%d connect is err :%s and closed\n",strerror(errno));
			close(fd);
			return -1;
		}
		else
		{
			pstServInfo->stConn.iConnState  = CONNECTING;
			printf("%s:%d %s:%d is connecting\n",__FILE__,__LINE__,\
				pstServParam->acIP,pstServParam->iPort);
		}
	}
	else
	{
		pstServInfo->stConn.iConnState  = CONNECTED;
		printf("%s:%d %s:%d is connected\n",__FILE__,__LINE__,\
				pstServParam->acIP,pstServParam->iPort);
	}
	pstServInfo->stConn.fd = fd ;
	pstServInfo->stConn.lastLiveTime = time(NULL);

	servSetActive(pstServInfo);

	if(pstServInfo->stConn.iConnState == CONNECTED && \
		pstServInfo->stParam.pstCallBack.sendAuth != NULL)
	{
		pstServInfo->stParam.pstCallBack.sendAuth(pstServInfo);
	}
	
	return 0;
}

int closeSocket(ServerInfo *pstServInfo)
{
	if(pstServInfo == NULL)
		return -1 ;
	printf("%s:%d  %s:%d is closed\n",__FILE__,__LINE__,\
		pstServInfo->stParam.acIP,pstServInfo->stParam.iPort);
	close(pstServInfo->stConn.fd) ;
	memset(&pstServInfo->stParam,0x00,sizeof(pstServInfo->stParam));

	servSetInActive(pstServInfo);
	
	return 0 ;
}

static int connrecv(ServerInfo *pstServInfo)
{
	int dataLen = 0 ;
	ServerConn *pstServConn = &pstServInfo->stConn ;
	
	dataLen = recv(pstServConn->fd,pstServConn->acbuf + pstServConn->iEndPos,\
		MSG_MAX_LEN+4 - pstServConn->iEndPos,0);

	if(dataLen < 0)
	{
		if(errno == EAGAIN || errno == EINTR || errno == EWOULDBLOCK)
		{
			printf("%s:%d %s:%d socket is err:%s\n",__FILE__,__LINE__,\
				pstServInfo->stParam.acIP,pstServInfo->stParam.iPort,strerror(errno));
			return 0 ;
		}
		else 
		{
			printf("%s:%d %s:%d socket is err:%s\n",__FILE__,__LINE__,\
				pstServInfo->stParam.acIP,pstServInfo->stParam.iPort,strerror(errno));
			pstServInfo->stParam.pstCallBack->close(pstServInfo);
			return -1 ;
		}
	}
	else if(dataLen == 0)
	{
		printf("%s:%d %s:%d socket is read the end\n",__FILE__,__LINE__,\
				pstServInfo->stParam.acIP,pstServInfo->stParam.iPort);
		pstServInfo->stParam.pstCallBack->close(pstServInfo);
		return -1 ;
	}
	else
	{
		printf("%s:%d socket:%s:%d recv data len:%d\n",__FILE__,__LINE__,\
			pstServInfo->stParam.acIP,pstServInfo->stParam.iPort,dataLen);
		pstServInfo->stConn.iEndPos += dataLen;
	}
	
	return dataLen ;
}

static void ConsumeMsg(ServerInfo *pstServInfo,int pktLen)
{

	if(pstServInfo == NULL)
	{
		printf("%s:%d ServerInfo is NULL\n");
		return ;
	}
	ServerConn *pstServConn = &pstServInfo->stConn ;
	ServerParam *pstServParam = &pstServInfo->stParam ;
	int id = 0 ;
	
	MsgPacket *pstPack = (MsgPacket*)malloc(sizeof(sizeof(MsgPacket)+ pktLen)) ;

	memset(pstPack,0x00,sizeof(MsgPacket));

	pstPack->sertype = pstServParam->iType ;
	pstPack->pstSvrInfo = pstServInfo;
	pstPack->len = pktLen ;
	memcpy(pstPack->data,pstServConn->acbuf + pstServConn->iStartPos,pktLen) ;
	pstPack->data[pktLen] = '\0' ;

	id = gProId++ % THREAD_NUMBER;

	pstServConn->iStartPos += pktLen ;
	msgEnque(&gMsgPool, (void *)pstPack, id)

	return ;
	
}

static int checkBuf(ServerConn *pstServConn)
{
	int dataLen = = 0 ;
	if(pstServConn == NULL)
	{
		printf("%s:%d err\n") ;
		return 0 ;
	}

	if(pstServConn->iEndPos == pstServConn->iStartPos)
	{
		pstServConn->iStartPos = pstServConn->iEndPos = 0 ;
	}

	if((pstServConn->iStartPos != 0 && pstServConn->iEndPos >= 2*MSG_MAX_LEN) || \
		(pstServConn->iStartPos >= 2*MSG_MAX_LEN))
	{
		dataLen = pstServConn->iEndPos - pstServConn->iStartPos ;
		memcpy(pstServConn->acbuf,pstServConn->acbuf + pstServConn->iStartPos,\
			dataLen) ;
		pstServConn->iEndPos = dataLen;
		pstServConn->iStartPos = 0 ;
	}

	if(dataLen !=  0)
	{
		return 1 ;
	}
	
	return 0 ; 
}

static int test_recv_data(ServerInfo *pstServInfo)
{
	int dataLen = 0 ;
	int pktLen = 0 ;
	int bodyLen = 0 ;
	ServerConn *pstServConn = &pstServInfo->stConn ;
	
	if( -1 == connrecv(pstServInfo))
	{
		return -1 ;
	}
	
Again:
	dataLen = pstServConn->iEndPos - pstServConn->iStartPos ;

	if(dataLen < 2)
	{
		printf("%s:%d message head not receice complete msg head,continue receice \n",\
			__FILE__,__LINE__);
		return -1 ;
	}

	memcpy(&bodyLen,pstServConn->acbuf + pstServConn->iStartPos,2) ;
	pktLen = ntohs(bodyLen) ;

	if(dataLen < pktLen)
	{
		printf("%s:%d message is not receice complete receiced dataLen=%d,need to receice is %d\n",\
			__FILE__,__LINE__,dataLen,pktLen-dataLen);
		return -1 ;
	}

	ConsumeMsg(pstServInfo,pktLen) ;

	if(checkBuf(pstServConn))
	{
		goto Again ;
	}

	return 0 ;
	
	
}

int dealTest(ServerInfo *pstServInfo,int events)
{
	if(events & (POLLIN|POLLERR)
	{
		test_recv_data(pstServInfo);
	}

	return 0 ;
}

int sendMsgTcp(ServerInfo *pstServInfo,char *buf,int len)
{
	ServerConn *pstServConn = NULL ;
	int needWrite = len ;
	int writed = 0 ;
	
	if(!pstServInfo || !buf)
	{
		printf("%s:%d pstServInfo or buf is NULL\n",__FILE__,__LINE__) ;
		return -1 ;
	}

	pstServConn = &pstServInfo->stConn ;
	while(needWrite >0)
	{
		writed = send(pstServConn->fd,buf,needWrite,0) ;
		if(writed < 0)
		{	
			if(errno == EINTR || errno== EAGAIN || errno == EWOULDBLOCK)
			{
				fd_set writeFds ;
				struct timeval tv ;
				tv.tv_sec = 1 ;
				tv.tv_usec = 0 ; 
				
				FD_ZERO(&writeFds) ;
				FD_SET(&writed,pstServConn->fd) ;
				select(pstServConn->fd +1,NULL,&writeFds,NULL,&tv) ;
			}
			else
			{
				printf("%s:%d %s:%d write is err:%s\n",__FILE__,__LINE__,\
					pstServInfo->stParam.acIP,pstServInfo->stParam.iPort,strerror(errno));
				pstServInfo->stParam.pstCallBack->close(pstServInfo);
				return -1 ;
			}
		}
		else if(writed == 0)
		{
			break ;
		}
		else
		{
			if(writed < needWrite)
			{
				buf + = writed ;
				needWrite -= writed ;
				continue ;
			}
			else
			{
				printf("%s:%d write len:%d\n",__FILE__,__LINE__,len) ;
				break ;
			}
		}
	}
	return len ;
}

int sendHbTest(ServerInfo *pstServInfo)
{
	int ret = 0 ;
	ServerParam *pstServParam = &pstServInfo->stParam;
	char buf[4] ;

	memset(buf,0x00,4) ;

	ret = pstServParam->pstCallBack->send(pstServInfo,buf,4) ;

	if(ret ==4)
	{
		printf("%s:%d send hb to %s:%d is ok\n",__FILE__,__LINE__,\
			pstServInfo->stParam.acIP,pstServInfo->stParam.iPort) ;
		return 0 ;
	}
	else
	{
		printf("%s:%d send hb to %s:%d is failed\n",__FILE__,__LINE__,\
			pstServInfo->stParam.acIP,pstServInfo->stParam.iPort) ;
		return -1 ;
	}
}

