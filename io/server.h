#ifndef _SERVER_H
#define _SERVER_H

#include "list.h"

#define MSG_MAX_LEN 1024
#define SERV_HEART_BEAT_TIME 3
#define MAX_IP_LENGTH 16
typedef struct 
{
    char acServName[32];
    char acIP[MAX_IP_LENGTH];
    int ipValue;
    int iPort;
    int iId;
}ServConf;

#define SERV_TEST "servTest"

typedef enum 
{
	SER_TYPE_TEST=1,
	SER_TYPE_BUTT
}ServerType_E ;

typedef struct serverCallBack ServerCallBack ;

typedef struct 
{
	int iType ;			// server type
	char acServName[32] ;	// server name
	char acIP[MAX_IP_LENGTH] ;
	int iIpInt ;
	int iPort ;
	int id ;
	ServerCallBack *pstCallBack ;
}ServerParam;

typedef enum
{
	CONNECTING,
	AUTHORING,
	AUTHORIZED,
	CONNECTED,
	DISCONNECT,
}ConnState_E ;

typedef struct 
{
	int fd  ;
	time_t lastLiveTime ;
	char acbuf[MSG_MAX_LEN+4] ;// the receive buffer
	int iStartPos ;	  // start buffer
	int iEndPos ;	  // the buffer end 
	int iRevents ;	  // 返回事件
	int iConnState ; // 连接状态
}ServerConn;

typedef struct 
{
	ServerConn stConn ;
	ServerParam stParam ;
}ServerInfo;

typedef struct 
{
	struct listHead stList ;
	ServerInfo stServInfo ;
}ServerNode;

typedef struct 
{
	struct listHead stHead ;
	pthread_rwlock_t lock ;
}ServerHead;

typedef struct 
{
	ServerHead stServArr[SER_TYPE_BUTT] ;
}ServerActive;

typedef struct 
{
	ServerCallBack stCallBack[SER_TYPE_BUTT] ;
	long lNum ;// server number ;
	ServerActive stActive ;
	struct listHead stServInactive ;
	struct listHead stServDel ;
}Servers;

typedef int (*dealEvtFunc)(ServerInfo *pstSerInfo,int event) ;
typedef int (*sendFunc)(ServerInfo *pstSerInfo,char *buf,int len) ;
typedef int (*sendHbFunc)(ServerInfo *pstSerInfo) ;
typedef int (*openConnFunc)(ServerInfo *pstServInfo) ;
typedef int (*closeConnFunc)(ServerInfo *pstServInfo) ;
typedef int (*sendAuthFunc)(ServerInfo *pstSerInfo) ;

struct serverCallBack
{
	openConnFunc openConn ;
	sendAuthFunc sendAuth ;
	sendFunc     send ;
	sendHbFunc   sendHb ;
	dealEvtFunc  dealEvt ;
	closeConnFunc close ;
} ;

int servInit() ;
void servSetActive(ServerInfo *pstServInfo) ;
void servSetInActive(ServerInfo *pstServInfo) ;
ServerInfo * servGetActiveServByTypeId(int iType,int iId) ;
ServerInfo * servGetActiveServByType(int iType) ;
void servCheckAndSendHeartBeat() ;
void servActiveCheck() ;
long servGetAllActiveServNum(ServerInfo **ppstServInfo,long maxServNum);
int servGetAllInactiveServNum(ServerInfo **ppstServInfo,long lMaxServNum) ;
long servGetAllServNum() ; 
#endif