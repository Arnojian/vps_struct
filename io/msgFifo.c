#include "msgFifo.h"




int sendMsg(Package *pPackage)
{
	ServInfo *pServInfo = NULL ;
	if(NULL == pPackage)
	{
		printf("%s:%d pPackage is NULL\n",__FILE__,__LINE__) ;
		return ;
	}

	pServInfo = pPackage->pstServInfo ;
	if(NULL == pServInfo)
	{
		printf("%s:%d pServInfo is NULL\n",__FILE__,__LINE__) ;
		return ;
	}

	if(pServInfo->stConn.iConnState == CONNECTED || pServInfo->stConn.iConnState == AUTHORIZED)
	{
		return pServInfo->stParam.pstCallBack.send(pServInfo,pPackage->data,pPackage->len) ;
	}

	printf("%s server %s:%d not connect\n",pServInfo->stParam.acServName,pServInfo->stParam.acIP,\
		pServInfo->stParam.iPort) ;
	return -1 ;
	
}

void freePackage(Package *pPackage)
{
	if(NULL == pPackage )
		return ;
	if(NULL != pMsg->data)
		free(pMsg->data) ;
	free(pMsg) ;
}

void dealPollEventPipe(int fd,int revents)
{
	char buf[32] ;
	int n = 0 ;
	void *pMsg = NULL ;

	if(revents &(POLLIN|POLLERR) )
	{
		n = read(fd,buf,32) ;
		if(n <=0)
		{
			printf("%s:%d send fd error\n",__FILE__,__LINE__) ;
			return ;
		}

		while(n >0)
		{
			sendMsg((packet*)pMsg) ;
			freePackage(pMsg) ;
			n-- ;
		}
	}
}

MsgFifo_t *msgFifoCreate(int maxNodes,freeNodeFunc freeNode_f)
{
	MsgFifo_t *pstMsgFifo = NULL ;
	MsgNode_t *pstMsgNode = NULL ;
	int i = 0 ;

	pstMsgFifo = (MsgFifo_t *)malloc(sizeof(MsgFifo_t)) ;

	if(NULL == pstMsgFifo)
	{
		printf("pstMsgFifo is NULL\n") ;
		return NULL ;
	}
	
	memset(pstMsgFifo,0,sizeof(MsgFifo_t)) ;

	pstMsgFifo->pMalloc = (char *)malloc(maxNodes * sizeof(MsgNode_t)) ;
	if(pstMsgFifo->pMalloc==NULL)
	{
		printf("MsgFifo pMalloc is NULL\n") ;
		free(pstMsgFifo);
		return NULL;
	}
	memset(pstMsgFifo->pMalloc,0,maxNodes*sizeof(MsgNode_t)) ;
	
	pstMsgFifo->freeNodeList = (MsgNode_t *)pstMsgFifo->pMalloc ;
	pstMsgNode = pstMsgFifo->freeNodeList ;

	for(i = 0 ;i<maxNodes-1;i++)
	{
		pstMsgNode->next = pstMsgNode + 1 ;
		pstMsgNode ++ ;
	}
	pstMsgNode->next = NULL ;

	pstMsgFifo->maxNodes = maxNodes ;
	pstMsgFifo->nodesNum = 0 ;
	pstMsgFifo->freeNode_f = freeNode_f ;

	return pstMsgFifo;
}

static MsgNode_t* allocMsgNode(MsgFifo *pstFifo)
{
	MsgNode_t *pstMsgNode = NULL ;
	
	if(NULL == pstFifo->freeNodeList)
		return NULL ;
	
	pstMsgNode = pstFifo->freeNodeList ;

	pstFifo->freeNodeList = pstFifo->freeNodeList->next ;
	
	memset(pstMsgNode,0,sizeof(MsgNode_t)) ;

	return pstMsgNode ;
}

int msgFifoPush(MsgFifo *pstFifo,void *data)
{
	MsgNode_t *pstMsgNode = NULL ;

	if(NULL != pstFifo || NULL == data)
	{
		printf("%s:%d fifo push is error,pstFifo is NULL\n",__FILE__,__LINE__) ;
		return -1 ;
	}

	if(pstFifo->nodesNum >= pstFifo->maxNodes)
	{
		printf("%s:%d the fifo is full\n",__FILE__,__LINE__) ;
		return -1 ;
	}

	pstMsgNode = allocMsgNode(pstMsgFifo) ;

	pstMsgNode->data = data ;
	
	if(NULL == pstMsgFifo->pstHead)
	{
		pstMsgFifo->pstHead = pstMsgNode ;
		pstMsgFifo->tail = pstMsgNode ;
	}
	else
	{
		pstMsgFifo->tail->next = pstMsgNode ;
		pstMsgFifo->tail = pstMsgNode ;
	}
	printf("%s:%d one mesg is pushed in the fifo \n",__FILE__,__LINE__);
	
	return 0 ;

}

void * msgFifoPop(MsgFifo_t *pstFifo)
{
	
	MsgNode_t *pstMsgNode = NULL ;

	if(NULL == pstFifo)
	{
		printf("%s:%d fifo is NULL\n",__FILE__,__LINE__);
		return NULL ;
	}
	
	if(NULL == pstFifo->pstHead)
	{
		printf("%s:%d fifo have no msg\n") ;
		return NULL ;
	}
	
	pstMsgNode = pstFifo->pstHead ;

	pstFifo->pstHead = pstFifo->pstHead->next ;
	pstFifo->nodesNum -- ;

	if(NULL == pstFifo->pstHead) // the head will be null
	{
		pstFifo->pstTail = NULL ;
		pstFifo->nodesNum = 0 ;
	}

	return pstMsgNode->data ;

}

void * msgFifoPeek(MsgFifo_t *pstMsgFifo)
{
	if(NULL == pstMsgFifo->pstHead)
	{
		return NULL ;
	}

	return pstMsgFifo->pstHead->data ;
}

void msgFifoFree(MsgFifo_t *pstMsgFifo)
{
	if(NULL == pstMsgFifo)
	{
		printf("%s:%d pstMsgFifo is NULL\n",__FILE__,__LINE__) ;
		return ;
	}

	

	if(pstMsgFifo->pMalloc)
	{}
}