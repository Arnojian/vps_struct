#ifndef _LIST_H
#define _LIST_H

struct listHead
{
	struct listHead *next ;
	struct listHead *prev ;
};

// 获得宿主结构体的指针
#define listEntry(ptr,type,member) (  (type*)( (char *)ptr - (unsigned long)(&( (type *)0)->member) ) )

// 链表遍历
#define listForEach(pos,head) ( for(pos=(head->next);pos != (head); pos=pos->next )

// 宿主遍历
#define listForEachEntry(pos,head,member) \
	for(pos = listEntry((head->next),typeof(*pos),member) ;  \
		&pos->member != (head) ;		\
		pos = listEntry( (pos->member.next),typeof(*pos),member)) 

void listInit(struct listHead *pList) ;

void listAddHead(struct listHead *pHead,struct listHead *pNew) ;

void listAddTail(struct listHead *pHead,struct listHead *pNew) ;

void listMoveToTail(struct listHead *pHead,struct listHead *pNew) ;

void listDel(struct listHead *pEntry) ;

int listEmpty(const struct listHead *pHead) ;

struct listHead* listPeekHead(struct listHead* pHead) ;

struct listHead* listPeekTail(struct listHead* pHead) ;

void listSplice(struct listHead *pList,struct listHead* pHead) ;

#endif 