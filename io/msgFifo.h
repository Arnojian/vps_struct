#ifndef _MSG_FIFO_H_
#define _MSG_FIFO_H_

typedef struct 
{
    int sertype;
    union
    {
        ServerInfo *pstSvrInfo; 
        int lId;    //sertype == SIPEX_SER_TYPE_TIME
    };
    int  	len;
	int 	threadId;
    char 	data[0];
}MsgPacket;

typedef struct msgNode
{
	struct msgFifo *next ;
	void *data ;
}MsgNode_t;

typedef struct msgFifo
{
	MsgNode_t *pstHead ;
	MsgNode_t *pstTail ;

	MsgNode_t *freeNodeList ;
	char *pMalloc ;

	int nodesNum ;
	int maxNodes ;

	freeNodeFunc freeNode_f ;
}MsgFifo_t;

typedef struct package
{
	int servType ;
	union
	{
		ServInfo *pstServInfo ;
		int lId ;//Timer
	};
	int len ;
	int threadId ;
	char data[0] ; //
}Package;

MsgFifo_t *msgFifoCreate(int maxNodes,freeNodeFunc freeNode_t) ;

int msgFifoPush(MsgFifo_t *pFifo, void *data) ;

void * msgFifoPop(MsgFifo_t *pFifo) ;

void * msgFifoPeek(MsgFifo_t *pFifo) ;

void * msgFifoFree(MsgFifo_t *pFifo) ;

int msgFifoNodesNum(MsgFifo *pFifo) ;

void dealPollEventPipe(int fd,int revents) ;

#endif