#ifndef _WORK_THREAD_H_
#define _WORK_THREAD_H_

#define THREAD_NUMBER 10 
typedef int (*freeNodeFunc) (void *,const char *file,int line);
typedef int (* WorkThreadCB)(void *pMsg,int threadNum) ;

typedef struct msgQueParams
{
	int maxFifoNodes ;
	workThreadCB cb ;
	pthread_mutex_t fifoMutex[THREAD_NUMBER] ;
	MsgFifo_t  *fifo[THREAD_NUMBER] ;

	pthread_mutex_t sendFifoMutex ;
	MsgFifo_t *sendFifo ;

	sem_t semaphore[THREAD_NUMBER] ;

	pthread_t workThreadNumber[THREAD_NUMBER] ;
	
	freeNodeFunc freeNode_f ;

	int pipefd[2] ;
	#define reader pipefd[0] 
	#define writer pipefd[1]

}MsgQueParams ;

void *msgProcess(void *pstMsg,int iId) ;

void msgPoolInit();

int msgEnque(MsgQueParams *pstMsgPool,void *pMsg,int iId) ;

void *msgDeque(MsgQueParams *pstMsgPool,int iId) ;

int msgEnqueTx(MsgQueParams *pstMsgPool,void *pMsg) ;

int msgDequeTx(MsgQueParams *pstMsgPool) ;

#endif