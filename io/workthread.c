#include "workthread.h"

MsgQueParams gMsgPool ;
#define MAX_NODE_NUM 10000

void *msgProcess(void *pstMsg,int iId) // msg main process
{
	Package *pstPackage = (Package*)pstMsg ;
	switch(pstPackage->servType)
	{
		case 1:
			printf("msg Process type = 1\n") ;
			break;
		default:
			printf(" msg process default \n") ;
	}

	return NULL;
}

static void *workThread(void *arg)
{
	int iId = int(arg) ;
	void *pMsg = NULL ;

	while(1) 
	{
		if(sem_wait(&gMsgPool.semaphore[iId])!=0)
		{
			if(errno == EINTR)
				continue ;
			else
			{
				break ;
			}
		}

		pMsg = msgDeque(&gMsgPool,iId) ;
		if(NULL == pMsg)
		{
			continue ;
		}
		else
		{
			gMsgPool.cb(pMsg,iId) ;
		}

	}// end while

	pthread_exit(NULL) ;
}

void msgPoolInit() 
{
	int i = 0 ;
	int iRet = 0 ;

	gMsgPool.maxFifoNodes = MAX_NODE_NUM ;
	gMsgPool.cb = msgProcess ;

	for(i  = 0;i<THREAD_NUMBER;i++)
	{
		if(pthread_mutex_init(&gMsgPool.fifoMutex[i],NULL) <0)
		{
			printf("%s:%d fifoMutex init is failed,err:%s:%d\n",\
				__FILE__,__LINE__,strerror(errno),errno) ;
			exit(-1) ;
		}

		gMsgPool.fifo[i] = msgFifoCreate(gMsgPool.maxFifoNodes,NULL) ;
		if(NULL == fifo[i])
		{
			printf("%s:%d msgFifoCreate is failed\n",__FILE__,__LINE__) ;
			exit(-1) ;
		}
		
		if(sem_init(&gMsgPool.semaphore[i],0,0)  < 0)
		{
			printf("%s:%d sem_init is failed\n",__FILE__,__LINE__) ;
			exit(-1) ;
		}

		if(pthread_create(&gMsgPool.workThreadNumber[i],NULL,workThread,(void*)i)< 0)
		{
			printf("%s:%d pthread_create is failed\n",__FILE__,__LINE__) ;
			exit(-1) ;
		}

	}

	iRet = pthread_mutex_init(&gMsgPool.sendFifoMutex,NULL) ;
	if(iRet <0)
	{
		printf("%s:%d sendFifoMutex init is failed \n",__FILE__,__LINE__) ;
		exit(-1) ;
	}

	sendFifo = msgFifoCreate(gMsgPool.maxFifoNodes,NULL) ;
	if(NULL == sendFifo)
	{
		printf("%s:%d sendFifo create failed \n",__FILE__,__LINE__) ;
		exit(-1) ;
	}
	
	if(pipe(gMsgPool.pipefd) < 0)
	{
		printf("%s:%d create pipe failed\n",__FILE__,__LINE__) ;
		exit(-1) ;
	}

	return ;
}

int msgEnque(MsgQueParams *pstMsgPool,void *pMsg,int iId)
{
	if(NULL == pstMsgPool || NULL == pMsg)
	{
		printf("%s:%d msg is NULL\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	
	pthread_mutex_lock(&pstMsgPool->fifoMutex[iId]) ;
	ret = msgFifoPush(pstMsgPool->fifo[i],pMsg) ;
	pthread_mutex_unlock(&pstMsgPool->fifoMutex[iId]) ;
		
	if(!ret)
	{
		sem_post(&pstMsgPool->semaphore[iId]) ;
		return 0 ;
	}
	else
	{
		return -1 ;
	}
}

void* msgDeque(MsgQueParams *pstMsgPool,int iId)
{
	void *pMsg = NULL ;

	if(NULL == pstMsgPool)
	{
		return NULL ;
	}

	pthread_mutex_lock(&pstMsgPool->fifoMutex[i]) ;
	pMsg = msgFifoPop(p->fifo[i]) ;
	pthread_mutex_unlock(&pstMsgPool->fifoMutex[i]) ;

	if(NULL == pMsg)
	{
		return NULL ;
	}
	else
	{
		return pMsg ;
	}

}

int msgEnqueTx(MsgQueParams *pstMsgPool,void *pMsg)
{
	int iRet = 0 ;
	pthread_mutex_lock(&pstMsgPool->sendFifoMutex) ;
	iRet = msgFifoPush(pstMsgPool->sendFifo,pMsg) ;
	pthread_mutex_unlock(&pstMsgPool->sendFifoMutex) ;

	if(!iRet)
	{
		write(pstMsgPool->writer,"w",1) ;
		return 0 ;
	}
	else
	{
		return -1 ;
	}
}

void *msgDequeTx(MsgQueParams *pstMsgPool)
{
	void *pMsg = NULL ;

	pthread_mutex_lock(&pstMsgPool->sendFifoMutex) ;
	pMsg = msgFifoPop(&pstMsgPool->sendFifo) ;
	pthread_mutex_unlock(&pstMsgPool->sendFifoMutex) ;

	if(pMsg)
	{
		return pMsg ;
	}
	else
	{
		return NULL ;
	}
}