#include "list.h"


void listInit(struct listHead *pList)
{
	pList->prev  = pList ;
	pList->next  = pList ;
}

void __listAdd(struct listHead *pNew,struct listHead *pPrev,struct listHead *pNext)
{
	pPrev->next = pNew ;
	pNew->next = pNext ;
	pNew->prev = pPrev ;
	pNext->prev = pNew ;
}

void listAddHead(struct listHead *pHead,struct listHead *pNew)
{
	__listAdd(pNew,pHead,pHead->next) ;
}

void listAddTail(struct listHead *pHead,struct listHead *pNew)
{
	__listAdd(pNew,pHead->prev,pHead) ;
}

void listMoveToTail(struct listHead *pHead,struct listHead *pNew)
{
	listDel(pNew) ;
	listAddTail(pHead,pNew) ;
}

void _listDel(struct listHead *pPrev,struct listHead *pNext)
{
	pPrev->next = pNext ;
	pNext->prev = pPrev ;
}

void listDel(struct listHead *pEntry)
{
	_listDel(pEntry->prev,pEntry->next) ;
	pEntry->next = NULL ;
	pEntry->prev = NULL ;
}

int listEmpty(struct listHead *pHead)
{
	return (pHead->next == pHead);
}

struct listHead *listPeekHead(struct listHead *pHead)
{
	if(listEmpty(pHead))
	{
		return NULL ;
	}
	return pHead->next ;
}

struct listHead *listPeekTail(struct listHead *pHead)
{
	if(listEmpty(pHead))
	{
		return NULL ;
	}
	return pHead->prev ;
}

void _listSplice(struct listHead *list,struct listHead *pHead)
{
	struct listHead *first = list->next ;
	struct listHead *last  = list->prev ;
	struct listHead *cur   = pHead->next ;

	first->prev = head ;
	head->next = first ;

	last->next = at ;
	at->prev = last ;
}

void listSplice(struct listHead *list,struct listHead *pHead) 
{
	if(!listEmpty(list)
	{
		_listSplice(list,pHead);
	}
}


