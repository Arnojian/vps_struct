#include "server.h"

static const char *servName[SER_TYPE_BUTT] = 
{
	"",
	SERV_TEST,
	"",
}

static Servers gstServers = {
	{
		{}, // 
		{
			openTcp;
			closeSocket;
			sendMsgTcp;
			dealTest;
			sendHbTest;
			NULL;
		},
	},
	
} ;

static ServerNode* servNewServNode()
{
	ServerNode *pstNode = NULL ;

	pstNode = (ServerNode *)malloc(sizeof(ServerNode)) ;
	if(NULL == pstNode)
	{
		printf("err %s:%d failed to malloc\n",__FILE__,__LINE__) ;
		return NULL ;
	}

	memset(pstNode,0,sizeof(ServerNode)) ;
	listInit(pstNode->stList) ;

	printf("new Node is init\n") ;

	return pstNode ;
}

int servInit() 
{
	ServerHead *pstServHead = NULL ;
	int i = 0 ;

	memset(&gstServers.stActive,0,sizeof(ServerActive)) ;
	
	for(i = 1;i< SER_TYPE_BUTT;i++)
	{
		pstServHead = &(gstServers.stActive.stServArr + i) ;
		listInit(pstServHead->stHead.stList) ;
		pthread_rwlock_init(&pstServHead->lock,NULL) ;
	}

	listInit(&gstServers.stServInactive) ;
	listInit(&gstServers.stServDel) ;

	return 0 ;
}

static int ServAddActiveServNode(ServerHead* pstHead,ServerNode *pstNode)
{
	if(NULL == pstHead || NULL == pstNode)
	{
		printf("pstHead==NULL %d, pstNode==NULL %d\n",NULL==pstHead,NULL==pstNode) ;
		return -1 ;
	}

	pthread_rwlock_wrlock(&pstHead->lock) ;
	listMoveToTail(pstHead->stHead,pstNode->stList) ;
	pthread_rwlock_unlock(&pstHead->lock) ;

	printf("%s:%d add the node:%s ip:%s,port:%d to pstHead is ok\n",\
		__FILE__,__LINE__,pstNode->stServInfo.stParam.acServName,\
		pstNode->stServInfo.stParam.acIP,pstNode->stServInfo.stParam.iPort) ;
	
	return 0 ;
}

void servSetActive(ServerInfo *pstServInfo)
{
	ServerNode *pstServNode = NULL ;
	ServerHead *pstServHead = NULL ;

	int iType = 0 ;
	if(NULL == pstServInfo)
	{
		printf("NULL == pstServInfo\n");
		return ;
	}

	iType  = pstServInfo->pstParam.iType ;
	pstServHead = gstServers.stActive.stServArr + iType ;
	pstServNode = listEntry(pstServInfo,ServerNode,stServInfo) ;
	
	ServAddActiveServNode(pstServHead,pstServNode) ;

	return ;
}

void servSetInActive(ServerInfo *pstServInfo)
{
	ServerNode *pstServNode = NULL ;
	ServerHead *pstServHead = NULL ;
	int iType = 0 ;

	iType = pstServInfo->stParam.iType ;
	pstServHead = gstServers.stActive.stServArr + iType ;
	pstServNode = listEntry(pstServInfo,ServerNode,stServInfo) ;
	
	pthread_rwlock_wrlock(&pstServHead->lock) ;
	listMoveToTail(&gstServers.stServInactive,&pstServNode->stList) ;
	pthread_rwlock_unlock(&pstServHead->lock) ;

	printf("%s:%d add inactive server, %s %s:%d",__FILE__,__LINE__,\
		pstServNode->stParam.acServName,pstServNode->stParam->acIP,\
		pstServNode->stParam.iPort) ;

	return ;
}

ServerInfo * servGetActiveNodeById(ServerHead *pstHead,int iId)
{
	struct listHead *pPos = NULL ;
	ServerNode *pstServNode = NULL ;

	pthread_rwlock_rdlock(&pstHead->lock) ;
		
	listForEach(pPos,pstHead)
	{
		pstServNode = listEntry(pPos,ServerNode,stList) ;
		if(pstServNode->stParam.id == iId)
		{
			printf("%s:%d find the server :%d\n",__FILE__,__LINE__,iId) ;
			pthread_rwlock_unlock(&pstHead->lock) ;
			return &pstServNode->stServInfo ;
		}
	}

	pthread_rwlock_unlock(&pstHead->lock) ;

	return NULL ;	
}

ServerInfo * servGetActiveServByTypeId(int iType,int iId)
{
	ServerNode *pstServNode = NULL ;
	ServerHead *pstServHead = NULL ; 
	
	pstServHead = gstServers.stActive.stServArr + iType ;
	
	return servGetActiveNodeById(pstServHead,iId) ;
}

ServerInfo * servGetActiveServById(int iId)
{
	int i = 0 ;
	ServerInfo * pstServInfo = NULL;
	for(i = 1;i<SER_TYPE_BUTT;i++)
	{
		pstServInfo = servGetActiveServByTypeId(i,iId) ;
		if(NULL == pstServInfo)
		{
			continue ;
		}
		else
		{
			return pstServInfo ;
		}
	}
	return NULL ;
}

static long servGetAllServNum() 
{
	return gstServers.lNum ;
}

static long servGetAllActiveServInfoByType(int iType,ServerInfo **ppstServInfo,long lMaxServNum)
{
	long lNum = 0 ;

	ServerHead *pstHead = NULL ;
	struct listHead *pPos = NULL ;
	ServerNode *pstServNode = NULL ;
	
	if(0 == lMaxServNum)
	{
		return 0 ;
	}

	if(NULL == ppstServInfo)
	{
		printf("%s:%d ppstServInfo is NULL\n",__FILE__,__LINE__) ;
		return -1 ;
	}
		
	*pstHead = gstServers.stActive.stServArr + iType ;

	pthread_rwlock_rdlock(&pstHead->lock) ;

	listForEach(pPos,pstHead)
	{
		pstServNode = listEntry(pPos,ServerNode,stList) ;
		*ppstServInfo = &pstServNode->stServInfo ;
		ppstServInfo ++ ;
		
		lNum ++ ;
		if(lNum == lMaxServNum)
		{
			break ;
		}
	}

	pthread_rwlock_unlock(&pstHead->lock) ;

	return lNum ;
}

static long servGetAllActiveServNum(ServerInfo **ppstServInfo,long maxServNum)
{
	long lNum = 0 ;
	int i = 0 ;
	int iRet = 0 ;

	if(ppstServInfo == NULL)
	{
		printf("NULL == ppstServInfo\n") ;
		return 0 ;
	}

	if(maxServNum == 0)
	{
		return 0 ;
	}

	for(i = 1;i< SER_TYPE_BUTT;i++)
	{
		iRet = servGetAllActiveServInfoByType(i,ppstServInfo,maxServNum-lNum) ;
		if(iRet < 0)
		{
			return iRet ;
		}
		else if(iRet == 0)
		{
			continue ;
		}
		else
		{
			lNum += iRet ;
			ppstServInfo += iRet ;
			if(lNum == maxServNum)
			{
				break ;
			} // end if lNum
		}// end if iRet
	}// end for i

	return lNum ;
}

void servCheckAndSendHeartBeat()
{
	ServerInfo ** ppstServInfo = NULL ;
	ServerInfo *  pstServInfo = NULL ;

	time_t tmNow = time(NULL) ;

	long lServNum = 0 ;
	long lActiveServNum = 0 ;

	lServNum = servGetAllServNum() ;
	ppstServInfo = (ServerInfo **)alloc(sizeof(ServerInfo*)*lServNum) ;
	
	if(NULL == ppstServInfo)
	{
		printf("%s:%d ppstServInfo is NULL,alloc is failed\n",__FILE__,__LINE__) ;
		return ;
	}

	lActiveServNum = servGetAllActiveServNum(ppstServInfo,lServNum);
	
	if(lActiveServNum <=0)
	{
		printf("%s:%d failed to get all active info\n") ;
		return ;
	}

	for(i = 0 ;i< lActiveServNum;i++)
	{
		pstServInfo = ppstServInfo[i] ;

		if( (pstServInfo->stParam.pstCallBack->sendHb == NULL) \
			|| (pstServInfo->stConn.lConnState < CONNECTED) )
		{
			continue ;
		}

		if(tmNow - pstServInfo->stConn.lastLiveTime > 3 * SERV_HEART_BEAT_TIME)
		{
			printf(" %s server ip %s:%d, heartbeat time is out\n",\
				pstServInfo->stParam.acServName,pstServInfo->stParam.acIP,\
				pstServInfo->stParam.iPort) ;

			pstServInfo->stParam.pstCallBack->close(pstServInfo);
			continue ;
		}

		pstServInfo->stParam.pstCallBack->sendHb(pstServInfo) ;
	}
		
	return ;
}

static int servDelNode(ServerNode *pstServNode)
{
	closeConnFunc close ;
	close = pstServNode->stParam.pstCallBack->close ;

	if(DISCONNECT != pstServNode->stServInfo.stConn.lConnState && close)
	{
		close(pstServNode->stServInfo) ;
	}

	listMoveToTail(&gstServers.stServDel,&pstServNode->stList);
	
	gstServers.lNum-- ;

	printf("%s:%d del Server %s %s:%d\n",\
		pstServNode->stServInfo.stParam.acServName,\
		pstServNode->stServInfo.stParam.acIP,\
		pstServNode->stServInfo.stParam.iPort) ;

	return 0;
}

static int servGetServTypeByServName(const char *servName)
{
	int iType = 0 ;
	int i = 0 ;
	for(i = 1 ;i<SER_TYPE_BUTT;i++)
	{
		if(strcmp(servName,servName[i])==0)
		{
			return i ;
		}
	}
	return -1 ;
}

static ServerNode* servGetDelByConfParam(ServConf *pstServConf)
{
	ServerNode *pstServNode = NULL ;
	ServerParam *pstParam = NULL ;
	struct listHead *pPos = NULL ;
	listForEach(pPos,&gstServers.stServDel) 
	{
		pstServNode=listEntry(pPos,ServerNode,stList) ;
		pstParam  = &pstServNode->stServInfo.stParam ;
		if(pstParam->iIpInt == pstServConf->ipValue && pstParam->iPort == pstServConf->iPort && \
			strcmp(pstParam->acIP,pstServConf->acIP)==0 && \
			0== strcmp(pstParam->acServName,pstServConf->acServName))
		{
			listDel(&pstServNode->stList) ;
			
			return pstServNode ;
		}
	}

	return NULL ;
}

static int servAddConfigServ(ServConf *pstServConf)
{
	ServerNode *pstServNode = NULL ;
	ServerParam *pstParam = NULL ;
	
	pstServNode = servGetDelByConfParam(pstServConf) ;
	if(NULL == pstServNode)
	{
		pstServNode = servNewServNode() ;
		if(NULL == pstServNode)
		{
			printf("%s:%d pstServNode is NULL\n",__FILE__,__LINE__) ;
			return -1 ;
		}

		pstParam = pstServNode->stServInfo.stParam ;
		strcpy(pstParam->acServName,pstServConf->acServName) ;
		strcpy(pstParam->acIP,pstServConf->acIP) ;
		pstParam->iIpInt = pstServConf->ipValue ;
		pstParam->iPort = pstServConf->iPort ;
		pstParam->iId = pstServConf->id ;
		pstParam->iType = servGetServTypeByServName(pstParam->acServName) ;
		
		if(pstParam->iType < 1 || pstParam->iType >= SER_TYPE_BUTT)
		{
			printf("%s:%d failed get server type by name %s\n",__FILE__,__LINE__,pstServConf->acServName) ;
			free(pstServNode);
			return -1 ;
		}
		pstParam->pstCallBack = gstServers.stCallBack[pstParam->iType] ;
	}

	pstParam = &pstServNode->stServInfo.stParam ;
	pstParam->lId = pstServConf->id ;
	
	listMoveToTail(&gstServers.stInactive,&pstServNode->stList) ;
	gstServers.lNum ++ ;
	
	printf("%s:%d add a config server, %s %s:%d\n",__FILE__,__LINE__,\
		pstParam->acServName,pstParam->acIP,pstParam->iPort) ;

	return 0 ;
}

static int servGetAllInactiveServNum(ServerInfo **ppstServInfo,long lMaxServNum) 
{
	long lNum = 0 ;
	struct listHead *pPos = NULL ;
	ServerNode *pstServNode = NULL ;

	if(ppstServInfo == NULL)
	{
		printf("%s:%d ppstServInfo is NULL\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	
	if(lServNum ==0)
	{
		return 0 ;
	}

	listForEach(pPos,&gstServers.stServInactive)
	{
		pstServNode =listEntry(pPos,ServerNode,stList) ;
		*ppstServInfo = pstServNode->stServInfo ;
		ppstServInfo ++ ;
		lNum ++ ;
		if(lNum == lMaxServNum)
		{
			break ;
		}
	}
	return lNum ;
}

void servActiveCheck()
{
	ServerInfo **ppstServInfo = NULL ;
	ServerParam *pstServParam = NULL ;

	long lServNum = 0 ;
	int i = 0 ;

	lServNum = servGetAllServNum() ;
	ppstServInfo = (ServerInfo **)alloc(sizeof(ServerInfo*)*lServNum) ;
	if(ppstServInfo == NULL)
	{
		printf("%s:%d failed alloc ServerInfo **\n",__FILE__,__LILNE__) ;
		return ;
	}

	lServNum = servGetAllInactiveServNum(ppstServInfo,lServNum) ;
	
	for(i = 0 ;i<lServNum;i++)
	{
		pstServParam = &(ppstServInfo[i].stParam) ;
		if(NULL == pstServParam->pstCallBack)
		{
			printf("%s:%d NULL == pstCallBack\n",__FILE__,__LINE__);
			return ;
		}

		openConnFunc *openCallBack = pstServParam->pstCallBack->openConn ;
		if(openCallBack)
		{
			openCallBack(ppstServInfo[i]) ;
		}
	}

	return ;
}