
#include "server.h"
#include "workthread.h"

unsigned int gProId = 0 ;
static int init()
{
	int iRet = 0 ;	
	iRet = servInit() ; 
	// timer
	// mysql
	// session
	// ....

	msgPoolInit() ;

	return iRet ;
}

static int socketStateCheck(int sock) // 检测连接是否发生错误
{
	socketlen_t n ;
	int error ;
	int iRet = 0 ;
	n = sizeof(error) ;
	
	iRet = getsockopt(sock,SOL_SOCKET,SO_ERROR,&error,&n) ;
	if(iRet <0 || error != 0)
	{
		return 1 ;
	}
	return 0 ;
}

static int dealPollEvent(ServInfo *pstServInfo)
{
	int iRet = 0;
	ServerParam *pstServParam = &pstServInfo->stParam ;
	if(NULL == pstServInfo)
	{
		printf("%s:%d servInfo is NULL\n",__FILE__,__LINE__) ;
		return -1 ;
	}

	pstServInfo->stConn.lastLiveTime = time(NULLL) ;

	if( CONNECTING == pstServInfo->stConn.iConnState)
	{
		if( pstServInfo->stConn.iRevents & (POLLIN |POLLOUT)) 
		{
			if(sockStateCheck(pstServInfo->stConn.fd)) // 异步IO,可能连接正在进行，所以需要检测连接是否已经成功
			{
				printf("%s:%d %s:%d socket %d,cannot connect,%s[%d]\n",\
					__FILE__,__LINE__,pstServParam->acIP,\
					pstServInfo->stParam->iPort,strerror(errno),errno) ;
				return -1 ;
			}

			pstServInfo->stConn.iConnState = CONNECTED ;
		}
	}
	else
	{
		if(pstServParam->pstCallBack->dealEvt)
		{
			iRet = pstServParam->pstCallBack->dealEvt(pstServInfo,pstServInfo->stConn.iRevents) ;
		}
	}
	pstServInfo->stConn.iRevents = 0 ;
	return iRet ;
}


void mainPoll() 
{
	ServerInfo **ppstServInfo = NULL ;
	struct pollfd *pstPollFd = NULL ;
	
	long lServNum = 0 ;
	int  iTimeOut = 1000 ;
	int  i = 0 ;

	lServNum = servGetAllServNum() ;
	ppstServInfo = (ServerInfo **)alloc(sizeof(ServerInfo*)*(lServNum+1)) ;

	memset(ppstServInfo,0,sizeof(lServNum+1)) ;

	lServNum = servGetAllActiveServNum(ppstServInfo+1,lServNum) ;
		
	if(lServNum <0 )
	{
		printf("failed to get all active info \n") ;
		return ;
	}

	pstPollFd = (struct pollfd*)alloc((lServNum+1)*sizeof(struct pollfd)) ;
	memset(pstPollFd,0,(lServNum+1)*sizeof(structpollfd)) ;

	pstPollFd[0].fd = gMsgPoll.reader ;
	pstPollFd[0].events = (POLLIN | POLLPRI) ;
	pstPollFd[0].revents = 0 ;

	for(i = 1 ;i<=lServNum;i++)
	{
		pstPollFd[i].fd = ppstServInfo[i]->stConn.fd ;
		pstPollFd[i].events = (ppstServInfo[i]->stConn.iConnState == CONNECTING) ? (POLLIN | POLLPRI | POLLOUT) :(POLLIN|POLLPRI) ;
		pstPollFd[i].revents = 0 ;
	}

again:
	switch(poll(pstPollFd,lServNum+1,iTimeOut))
	{
		case -1:
		{
			if(errno == EINTR)
			{
				goto again ;
			}
			break ;
		}
		case 0:
		{
			break ;
		}
		default:
		{
			if(pstPollFd[0].revents)
			{
				dealPollEventPipe(pstPollFd[0].fd,pstPollFd[0].revents) ;
			}

			for(i = 1 ;i<=lServNum;i++)
			{
				if(pstPollFd[i].revents)
				{
					ppstServInfo[i]->stConn.revents = pstPollFd[i].revents ;
					dealPollEvent(ppstServInfo[i]) ;
				}
			}
		}
	}

	return ;
}

int mainServ()
{
	
	time_t checkTime = 0 ;
	time_t heartBeatTime= 0 ;
	time_t now = 0 ;

	while(1)
	{
		now = time(NULL) ;

		if( (now - checkTime) >= SERV_CHECK_TIME)
		{
			servActiveCheck() ;
			checkTime = now ;
		}

		if( (now - heartBeatTime)>= SERV_HEARTBEAT_TIME)
		{
			servCheckAndSendHeartBeat() ;
			heartBeatTime = now ;
		}

		mainPoll() ;

	}

	return 0 ;
}

int main()
{
	if(init() != 0)
	{
		printf("%s:%d init is error\n",__FILE__,__LINE__) ;

		return -1 ;
	}
	
	mainServ() ;

	destroy() ;

	return 0 ;
}