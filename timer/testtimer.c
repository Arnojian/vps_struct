#include "timer.h"

#include <stdio.h>
#include <stdlib.h>

int callbackTimer(void *data)
{
	printf("callback Timer") ;
	return 1 ;
}

int main()
{
	int timerId ;
	timerProcStart() ;
	registerTimer(10,callbackTimer,NULL) ;
	registerTimer(15,callbackTimer,NULL) ;
	registerTimer(20,callbackTimer,NULL) ;
	timerId = registerTimer(25,callbackTimer,NULL) ;
	registerTimer(30,callbackTimer,NULL) ;
	//cancelTimer(timerId) ;

	sleep(50) ;
	printf("main is exit\n") ;

	return 0 ;
}