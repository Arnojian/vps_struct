#include "timer.h"
#include <sys/time.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>

Timer_S gTimer ;

int		initTimer();
void	destroyTimer() ;

static unsigned int getTicks()
{
	return time(NULL) ;
}

static inline void removeTimerUnSafe(TimerNode_S *pstTimerNode)
{
	pstTimerNode->prev->next = pstTimerNode->next ;
	pstTimerNode->next->prev = pstTimerNode->prev ;
}

static inline void insertTimerUnSafe(TimerList_S *pstTimerList,TimerNode_S *pstNewTimer) 
{
	TimerNode_S *pstTimerNode = pstTimerList->head.next ;
	
	for(;pstTimerNode != &pstTimerList->head ;pstTimerNode=pstTimerNode->next)
	{
		if(pstTimerNode->timeOut <= pstNewTimer->timeOut)
		{
			printf("%d ",pstTimerNode->id) ;
			continue ;
		}
		else
		{
			break ;
		}
	}

	pstTimerNode->prev->next = pstNewTimer ;
	pstNewTimer->prev = pstTimerNode->prev ;
	pstNewTimer->next = pstTimerNode ;
	pstTimerNode->prev = pstNewTimer ;

	return ;
}


/*
	free the timer Node
*/
static inline void freeTimerNode(TimerNode_S *pstTimerNode)
{
	if(pstTimerNode->para == NULL)
	{
		return ;
	}
	else
	{
		printf("%s:%d free the TimerNode %d\n",__FILE__,__LINE__,pstTimerNode->id);
		if(pstTimerNode->para->data != NULL)
		{
			free(pstTimerNode->para->data) ;
			pstTimerNode->para->data = NULL ;
		}
		else
		{
			free(pstTimerNode->para) ;
			pstTimerNode->para = NULL ;
		}
	}
	
	free(pstTimerNode) ;
	pstTimerNode = NULL ;
	return ;
}

/*
	remove the Timer list 
*/
static inline void removeTimerList(TimerList_S *pstTimerList)
{
	TimerNode_S *pTimerNode = pstTimerList->head.next;
	
	pthread_mutex_lock(&pstTimerList->lock) ;
	
	while(pTimerNode != &pstTimerList->head)
	{
		
		removeTimerUnSafe(pTimerNode) ;
		freeTimerNode(pTimerNode) ;
		pTimerNode = pstTimerList->head.next ;
	}

	pthread_mutex_unlock(&pstTimerList->lock) ;

	pthread_mutex_destroy(&pstTimerList->lock) ;

	return ;
}

/*
	init the timer
*/
int		initTimer()
{
	int ret = 0 ;
	memset(&gTimer,0x00,sizeof(Timer_S)) ;
	ret = pthread_mutex_init(&gTimer.lock,NULL) ;
	if(ret <0)
	{
		printf("%s:%d, timer init lock is error\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	
	
	ret = pthread_mutex_init(&gTimer.secondTimer.lock,NULL) ;
	if(ret < 0)
	{
		pthread_mutex_destroy(&gTimer.lock) ;
		printf("%s:%d, second timer lock init is error\n",__FILE__,__LINE__) ;
		return -1 ;
	}
		
	gTimer.secondTimer.head.next = gTimer.secondTimer.head.prev = &gTimer.secondTimer.head ;

	
	ret = pthread_mutex_init(&gTimer.uSecondTimer.lock,NULL) ;
	if(ret < 0)
	{
		pthread_mutex_destroy(&gTimer.secondTimer.lock) ;
		pthread_mutex_destroy(&gTimer.lock) ;
		printf("%s:%d, uSecond timer lock init is error\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	
	gTimer.uSecondTimer.head.next = gTimer.uSecondTimer.head.prev = &gTimer.uSecondTimer.head ;
	gTimer.cRun = 1 ;

	printf("%s:%d timer init is ok!!!!\n",__FILE__,__LINE__) ;

	return 0 ;
}

/*
	destory the timer
*/
void destroyTimer()
{
	printf("%s:%d,destroy the timer is begin \n",__FILE__,__LINE__) ;

	removeTimerList(&gTimer.secondTimer) ;
	printf("%s:%d,destroy the second timer is ok\n",__FILE__,__LINE__) ;

	removeTimerList(&gTimer.uSecondTimer) ;
	printf("%s:%d,destroy the uSecond timer is ok\n",__FILE__,__LINE__) ;

	pthread_mutex_destroy(&gTimer.lock) ;

	printf("%s:%d,destroy the timer is ok\n",__FILE__,__LINE__) ;

	return ;
}

static inline int addTimerNodeToList(TimerList_S *pstTimerList,TimerNode_S *pstNewTimerNode)
{
	pthread_mutex_lock(&pstTimerList->lock) ;
	insertTimerUnSafe(pstTimerList,pstNewTimerNode) ;
	pthread_mutex_unlock(&pstTimerList->lock) ;
	return 0 ;
}

int registerTimer(int seconds,TimerFunc func,TimerPara_S *data) 
{
	TimerNode_S *pstNewTimerNode = NULL ;

	pstNewTimerNode = (TimerNode_S*)malloc(sizeof(TimerNode_S)) ;
	pstNewTimerNode->timeOut = getTicks()+seconds ;
	pstNewTimerNode->func = func ;
	pstNewTimerNode->para = data ;

	pthread_mutex_lock(&gTimer.lock) ;
	++gTimer.mNextId ;
	if(gTimer.mNextId %2 ==1)
	{
		pstNewTimerNode->id = gTimer.mNextId ;
	}
	else
	{
		pstNewTimerNode->id = ++gTimer.mNextId ; 
	}

	pthread_mutex_unlock(&gTimer.lock) ;

	addTimerNodeToList(&gTimer.secondTimer,pstNewTimerNode);
	
	printf("%s:%d, register timer is ok; the new timer id :%d\n",__FILE__,__LINE__,pstNewTimerNode->id) ;

	return pstNewTimerNode->id ;
}

static  TimerNode_S* findByTimerId(TimerList_S *pstTimerList,int timerId)
{
	TimerNode_S *pstTimerNode = pstTimerList->head.next ;
	for(;pstTimerNode != &pstTimerList->head ;pstTimerNode = pstTimerNode->next)
	{
		printf("%d ",pstTimerNode->id) ;
		if(pstTimerNode->id == timerId)
		{
			printf("%s:%d find the timer by id:%d\n",__FILE__,__LINE__,timerId) ;
			return pstTimerNode ;
		}
	}
	printf("%s:%d cannot find the timer by id:%d\n",__FILE__,__LINE__,timerId) ;
	return NULL ;
}

int cancelTimer(int timerId)
{
	TimerNode_S *pstTimerNode = NULL ;
	TimerList_S *pstTimerList = NULL ;
	if(timerId%2==1)
	{
		pstTimerList = &gTimer.secondTimer ;
	}
	else
	{
		pstTimerList = &gTimer.uSecondTimer ;
	}

	pthread_mutex_lock(&pstTimerList->lock) ;

	pstTimerNode=findByTimerId(pstTimerList,timerId) ;
	if(pstTimerNode != NULL )
	{
		removeTimerUnSafe(pstTimerNode) ;
		printf("%s:%d, cancelTimer timer is ok; the timer id :%d\n",__FILE__,__LINE__,timerId) ;
	}

	pthread_mutex_unlock(&pstTimerList->lock) ;

	return 0 ;
}

static inline TimerNode_S *timerGetExpire(unsigned int timeUInt,TimerList_S *pstTimerList)
{	
	TimerNode_S *pstTimerNode = NULL ;
	TimerNode_S *pCurTimerNode= pstTimerNode = pstTimerList->head.next ;

	
	pthread_mutex_lock(&pstTimerList->lock);
	if(pstTimerNode == &pstTimerList->head)
	{
		pthread_mutex_unlock(&pstTimerList->lock) ;
		return NULL ;
	}

	
	for(;pstTimerNode != &pstTimerList->head;pstTimerNode = pstTimerNode->next)
	{
		if(pstTimerNode->timeOut > timeUInt)
		{
			if(pstTimerNode == pCurTimerNode)
			{
				pCurTimerNode = NULL ;
			}
			else
			{
					pstTimerNode->prev->next = NULL ;
					pstTimerList->head.next = pstTimerNode ;
					pstTimerNode->prev = &pstTimerList->head ;
			}
			break ;
		}
		else
		{
			continue ;
		}
	}
	if(pstTimerNode == &pstTimerList->head)
	{
		pstTimerNode->prev->next = NULL;
		pstTimerList->head.prev = pstTimerList->head.next = &pstTimerList->head ;
	}
	
	pthread_mutex_unlock(&pstTimerList->lock) ;

	return pCurTimerNode ;
}

void *timerThread(void *arg)
{
	
	int ticks ;
	struct timeval tv ;
	TimerNode_S *pstTimerNode = NULL,*pstCurTimerNode = NULL  ;
	while(gTimer.cRun)
	{
		tv.tv_sec = 22 ;
		tv.tv_usec = 0 ;
		printf("one second is gone\n") ;
		select(0,NULL,NULL,NULL,&tv) ;
		pstTimerNode = timerGetExpire(getTicks(),&gTimer.secondTimer) ;
		printf("get expire is ok\n") ;
		while(pstTimerNode != NULL)
		{
			pstCurTimerNode = pstTimerNode ;
			pstTimerNode = pstTimerNode->next ;
			pstCurTimerNode->func(pstCurTimerNode->para) ;
			freeTimerNode(pstCurTimerNode) ;
		}
	}

	destroyTimer();
	pthread_exit(0) ;
}
int	    registerUTimer(int uSeconds,TimerFunc func,TimerPara_S *data) 
{
	return 0 ;
}
int timerProcStart()
{
	int ret =0;
	initTimer() ;

	pthread_t threadId;
	ret = pthread_create(&threadId,NULL,timerThread,NULL) ;
	if(ret != 0)
	{
		printf("%s:%d, pthread_create is error :%s\n",__FILE__,__LINE__,strerror(ret)) ;
		destroyTimer() ;
		return -1 ;
	}
	
	printf("%s:%d timer thread is runing !!!\n",__FILE__,__LINE__) ;

	return 0 ;
}