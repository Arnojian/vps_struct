#ifndef _TIMER_H
#define _TIMER_H

#include <pthread.h>

typedef int (*TimerFunc)(void* data) ;

typedef struct 
{
	int		timerType ;		// timer type
	void	*data ;			// param
}TimerPara_S;

typedef struct timerNode
{
	struct timerNode		*next;	
	struct timerNode		*prev;
	volatile unsigned int	timeOut;		// the expire time
	TimerFunc				func ;			// the call routine
	int						id ;			// timer id 
	TimerPara_S				*para ;			// the param 
}TimerNode_S;

typedef struct timerList
{
	TimerNode_S head ;		// the list of timer
	pthread_mutex_t	lock ;		// the lock 
}TimerList_S;

typedef struct timer
{
	TimerList_S		secondTimer ;	//�붨ʱ��
	TimerList_S		uSecondTimer ;	//΢�붨ʱ��
	unsigned int    mNextId ;   // the timer id
	unsigned int	cRun    ;
	pthread_mutex_t lock ;
}Timer_S;


int		registerTimer(int seconds,TimerFunc func,TimerPara_S *data) ;
int	    registerUTimer(int uSeconds,TimerFunc func,TimerPara_S *data) ;
int		cancelTimer(int timeId) ;
int		timerProcStart() ;

#endif
